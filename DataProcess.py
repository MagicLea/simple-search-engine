# coding=UTF-8

import re
import os
import timeit
import hashlib
import porterStemmer

__author__ = 'lea'

# Timer start
start = timeit.default_timer()

# main
stemmer = porterStemmer.PorterStemmer()
non_word = {}
buffer_token = {}
all_term_count = {}
for i in range(0xFFF + 1):
    buffer_token['%03x' % i] = {}
# 處理資料夾內所有檔案
count = 0
for folderName, subFolders, fileNames in os.walk('Data'):
    for fileName in fileNames:
        count += 1
        if (fileName.endswith('.txt')):
            print(os.path.join(folderName, fileName))
            file = open(os.path.join(folderName, fileName), encoding='utf-8')
            fileText = file.read()
            token_with_non_word = re.split('([A-Za-z]+|\W|[\u4E00-\u9FFF\u3400-\u4DFF]+|[0-9]+)', fileText)
            term_count = 0
            for i in token_with_non_word:
                if i:
                    term_count += 1
                    # 非文字
                    if re.match('\W', i) != None:
                        term_count -= 1
                        if non_word.get(i) == None:
                            # non_word.append(i)
                            non_word[i] = 0
                        non_word[i] += 1
                    else:
                        # 中文
                        if re.match('[\u4E00-\u9FFF\u3400-\u4DFF]+', i) != None:
                            # 1-Gram
                            for j in i:
                                tmp_hash = hashlib.md5(j.encode('utf-8')).hexdigest()[-3:]
                                # token.append(j)
                                if buffer_token[tmp_hash].get(j) == None:
                                    buffer_token[tmp_hash][j] = {}
                                if buffer_token[tmp_hash][j].get(fileName) == None:
                                    buffer_token[tmp_hash][j][fileName] = 0
                                buffer_token[tmp_hash][j][fileName] += 1
                            # 2-Gram
                            if (len(i) >= 2):
                                for j in range(len(i) - 1):
                                    tmp_hash = hashlib.md5(i[j:j + 2].encode('utf-8')).hexdigest()[-3:]
                                    # token.append(i[j:j+2])
                                    if buffer_token[tmp_hash].get(i[j:j + 2]) == None:
                                        buffer_token[tmp_hash][i[j:j + 2]] = {}
                                    if buffer_token[tmp_hash][i[j:j + 2]].get(fileName) == None:
                                        buffer_token[tmp_hash][i[j:j + 2]][fileName] = 0
                                    buffer_token[tmp_hash][i[j:j + 2]][fileName] += 1
                            # 3-Gram
                            if (len(i) >= 3):
                                for j in range(len(i) - 2):
                                    tmp_hash = hashlib.md5(i[j:j + 3].encode('utf-8')).hexdigest()[-3:]
                                    # token.append(i[j:j+3])
                                    if buffer_token[tmp_hash].get(i[j:j + 3]) == None:
                                        buffer_token[tmp_hash][i[j:j + 3]] = {}
                                    if buffer_token[tmp_hash][i[j:j + 3]].get(fileName) == None:
                                        buffer_token[tmp_hash][i[j:j + 3]][fileName] = 0
                                    buffer_token[tmp_hash][i[j:j + 3]][fileName] += 1
                        else:
                            # 英文
                            if re.match('[A-Za-z]+', i) != None:
                                term = stemmer.stem(i.lower(), 0, len(i) - 1)
                                tmp_hash = hashlib.md5(term.encode('utf-8')).hexdigest()[-3:]
                                # token.append(term)
                                if buffer_token[tmp_hash].get(term) == None:
                                    buffer_token[tmp_hash][term] = {}
                                if buffer_token[tmp_hash][term].get(fileName) == None:
                                    buffer_token[tmp_hash][term][fileName] = 0
                                buffer_token[tmp_hash][term][fileName] += 1
                            # 數字
                            else:
                                if re.match('[0-9]+', i) != None:
                                    tmp_hash = hashlib.md5(i.encode('utf-8')).hexdigest()[-3:]
                                    if buffer_token[tmp_hash].get(i) == None:
                                        buffer_token[tmp_hash][i] = {}
                                    if buffer_token[tmp_hash][i].get(fileName) == None:
                                        buffer_token[tmp_hash][i][fileName] = 0
                                    buffer_token[tmp_hash][i][fileName] += 1
                                # 其他神秘的文字
                                else:
                                    tmp_hash = hashlib.md5(i.encode('utf-8')).hexdigest()[-3:]
                                    if buffer_token[tmp_hash].get(i) == None:
                                        buffer_token[tmp_hash][i] = {}
                                    if buffer_token[tmp_hash][i].get(fileName) == None:
                                        buffer_token[tmp_hash][i][fileName] = 0
                                    buffer_token[tmp_hash][i][fileName] += 1
            all_term_count[fileName] = term_count
            # print('token',token)
            # print('buffer_token',buffer_token)
            # print(token_with_non_word)
            file.close()
            # 寫入資料庫
            if count >= 3000:
                print('writing database', end='')
                for i in buffer_token:
                    print('.', end='')
                    file = open(os.path.join('database', i + '.txt'), 'a', encoding='utf-8')
                    for j in sorted(buffer_token[i]):
                        for k in sorted(buffer_token[i][j]):
                            file.write('%s\t%s\t%d\n' % (j, k, buffer_token[i][j][k]))
                    file.close()
                print()
                for i in range(0xFFF + 1):
                    buffer_token['%03x' % i] = {}
                count = 0
# 清空buffer，寫入資料庫
if count > 0:
    print('writing database', end='')
    for i in buffer_token:
        print('.', end='')
        file = open(os.path.join('database', i + '.txt'), 'a', encoding='utf-8')
        for j in sorted(buffer_token[i]):
            for k in sorted(buffer_token[i][j]):
                file.write('%s\t%s\t%d\n' % (j, k, buffer_token[i][j][k]))
        file.close()
    print()
    for i in range(0xFFF + 1):
        buffer_token['%03x' % i] = {}
    count = 0
# 輸出statics
file = open(os.path.join('database', 'statics.txt'), 'a', encoding='utf-8')
for i in all_term_count:
    file.write('%s\t%d\n' % (i, all_term_count[i]))
file.close()

# print('buffer=',buffer_token)
print("non_word:", non_word)
file = open(os.path.join('database', 'non_word.txt'), 'a', encoding='utf-8')
# Timer stop
stop = timeit.default_timer()
file.write('eclipsed time:' + str((stop - start)))
for i in non_word:
    file.write(i + '\n')
file.close()

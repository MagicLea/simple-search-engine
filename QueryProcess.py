# coding=UTF-8

import re
import os
import timeit
import hashlib
import porterStemmer
import csv
import math
import logging

__author__ = 'lea'


def add(dict, doc, number):
    if (dict.get(doc) == None):
        if FIRST_TOKEN:
            dict[doc] = number
    else:
        dict[doc] += number


class ContextFilter(logging.Filter):
    def filter(self, record):
        return any([(i in record.msg) for i in GLOBAL_ANS])


# log
logging.basicConfig(format='[%(levelname)7s]\t%(message)s', level=logging.ERROR)
logging.root.addFilter(ContextFilter())


# main
stemmer = porterStemmer.PorterStemmer()
docs_count = {}
file = open(os.path.join('database', 'statics.txt'), encoding='utf-8')
for i in csv.reader(file, delimiter='\t'):
    docs_count[i[0]] = int(i[1])

while True:
    non_word = {}
    buffer_token = {}
    docs_score_tfidf = {}
    docs_score_tf = {}
    GLOBAL_ANS = ['']
    BUFFER_OUT = []
    FIRST_TOKEN = True
    fileText = '資料庫管理系統 高雄第一科技大學 摩托車發展史'
    try:
        fileText = input('請輸入查詢：')
    except(EOFError):
        break
    start = timeit.default_timer()
    token_with_non_word = re.split('([A-Za-z]+|\W|[\u4E00-\u9FFF\u3400-\u4DFF]+|[0-9]+)', fileText)
    for i in token_with_non_word:
        if i:
            # 非文字
            if re.match('\W', i) != None:
                pass
            else:
                # 中文
                if re.match('[\u4E00-\u9FFF\u3400-\u4DFF]+', i) != None:
                    # 3-Gram
                    if (len(i) >= 3):
                        for j in range(len(i) - 2):
                            tmp_hash = hashlib.md5(i[j:j + 3].encode('utf-8')).hexdigest()[-3:]
                            file = open(os.path.join('database', tmp_hash + '.txt'), encoding='utf-8')
                            reader = csv.reader(file, delimiter='\t')
                            logging.debug('search ' + i[j:j + 3])
                            term_result = {}
                            for k in reader:
                                if (k[0] == i[j:j + 3]):
                                    term_result.update({k[1]: int(k[2])})
                            for k in term_result:
                                score_TF = term_result[k] / docs_count[k]
                                score_TFIDF = term_result[k] / docs_count[k] * math.log10(
                                    len(docs_count) / len(term_result))
                                add(docs_score_tf, k, score_TF)
                                add(docs_score_tfidf, k, score_TFIDF)
                                BUFFER_OUT.append('3-Gram %s %s TFIDF:%d / %d * log(%d / %d)= %f TF:%f' % (
                                k, i[j:j + 3], term_result[k], docs_count[k], len(docs_count), len(term_result),
                                score_TFIDF, score_TF))
                            FIRST_TOKEN = False
                    # 2-Gram
                    elif (len(i) >= 2):
                        for j in range(len(i) - 1):
                            tmp_hash = hashlib.md5(i[j:j + 2].encode('utf-8')).hexdigest()[-3:]
                            file = open(os.path.join('database', tmp_hash + '.txt'), encoding='utf-8')
                            reader = csv.reader(file, delimiter='\t')
                            logging.debug('search ' + i[j:j + 2])
                            term_result = {}
                            for k in reader:
                                if (k[0] == i[j:j + 2]):
                                    term_result.update({k[1]: int(k[2])})
                            for k in term_result:
                                score_TF = term_result[k] / docs_count[k]
                                score_TFIDF = term_result[k] / docs_count[k] * math.log10(
                                    len(docs_count) / len(term_result))
                                add(docs_score_tf, k, score_TF)
                                add(docs_score_tfidf, k, score_TFIDF)
                                BUFFER_OUT.append('2-Gram %s %s TFIDF:%d / %d * log(%d / %d)= %f TF:%f' % (
                                k, i[j:j + 2], term_result[k], docs_count[k], len(docs_count), len(term_result),
                                score_TFIDF, score_TF))
                            FIRST_TOKEN = False
                    # 1-Gram
                    else:
                        for j in i:
                            tmp_hash = hashlib.md5(j.encode('utf-8')).hexdigest()[-3:]
                            file = open(os.path.join('database', tmp_hash + '.txt'), encoding='utf-8')
                            reader = csv.reader(file, delimiter='\t')
                            logging.debug('search ' + j)
                            term_result = {}
                            for k in reader:
                                if (k[0] == j):
                                    term_result.update({k[1]: int(k[2])})
                            for k in term_result:
                                score_TF = term_result[k] / docs_count[k]
                                score_TFIDF = term_result[k] / docs_count[k] * math.log10(
                                    len(docs_count) / len(term_result))
                                add(docs_score_tf, k, score_TF)
                                add(docs_score_tfidf, k, score_TFIDF)
                                BUFFER_OUT.append('1-Gram %s %s TFIDF:%d / %d * log(%d / %d)= %f TF:%f' % (
                                k, j, term_result[k], docs_count[k], len(docs_count), len(term_result), score_TFIDF,
                                score_TF))
                            FIRST_TOKEN = False
                else:
                    # 英文
                    if re.match('[A-Za-z]+', i) != None:
                        term = stemmer.stem(i.lower(), 0, len(i) - 1)
                        tmp_hash = hashlib.md5(term.encode('utf-8')).hexdigest()[-3:]
                        file = open(os.path.join('database', tmp_hash + '.txt'), encoding='utf-8')
                        reader = csv.reader(file, delimiter='\t')
                        logging.debug('search ' + term)
                        term_result = {}
                        for k in reader:
                            if (k[0] == term):
                                term_result.update({k[1]: int(k[2])})
                        for k in term_result:
                            score_TF = term_result[k] / docs_count[k]
                            score_TFIDF = term_result[k] / docs_count[k] * math.log10(
                                len(docs_count) / len(term_result))
                            add(docs_score_tf, k, score_TF)
                            add(docs_score_tfidf, k, score_TFIDF)
                            BUFFER_OUT.append('Eng %s %s TFIDF:%d / %d * log(%d / %d)= %f TF:%f' % (
                            k, term, term_result[k], docs_count[k], len(docs_count), len(term_result), score_TFIDF,
                            score_TF))
                        FIRST_TOKEN = False
                    # 數字
                    else:
                        if re.match('[0-9]+', i) != None:
                            tmp_hash = hashlib.md5(i.encode('utf-8')).hexdigest()[-3:]
                            file = open(os.path.join('database', tmp_hash + '.txt'), encoding='utf-8')
                            reader = csv.reader(file, delimiter='\t')
                            logging.debug('search ' + i)
                            term_result = {}
                            for k in reader:
                                if (k[0] == i):
                                    term_result.update({k[1]: int(k[2])})
                            for k in term_result:
                                score_TF = term_result[k] / docs_count[k]
                                score_TFIDF = term_result[k] / docs_count[k] * math.log10(
                                    len(docs_count) / len(term_result))
                                add(docs_score_tf, k, score_TF)
                                add(docs_score_tfidf, k, score_TFIDF)
                                BUFFER_OUT.append('Num %s %s TFIDF:%d / %d * log(%d / %d)= %f TF:%f' % (
                                k, i, term_result[k], docs_count[k], len(docs_count), len(term_result), score_TFIDF,
                                score_TF))
                            FIRST_TOKEN = False
                        # 其他神秘的文字
                        else:
                            tmp_hash = hashlib.md5(i.encode('utf-8')).hexdigest()[-3:]
                            file = open(os.path.join('database', tmp_hash + '.txt'), encoding='utf-8')
                            reader = csv.reader(file, delimiter='\t')
                            logging.debug('search ' + j)
                            term_result = {}
                            for k in reader:
                                if (k[0] == i):
                                    term_result.update({k[1]: int(k[2])})
                            for k in term_result:
                                score_TF = term_result[k] / docs_count[k]
                                score_TFIDF = term_result[k] / docs_count[k] * math.log10(
                                    len(docs_count) / len(term_result))
                                add(docs_score_tf, k, score_TF)
                                add(docs_score_tfidf, k, score_TFIDF)
                                BUFFER_OUT.append('Other %s %s TFIDF:%d / %d * log(%d / %d)= %f TF:%f' % (
                                k, i, term_result[k], docs_count[k], len(docs_count), len(term_result), score_TFIDF,
                                score_TF))
                            FIRST_TOKEN = False
    stop = timeit.default_timer()
    GLOBAL_ANS = ['']
    logging.debug('TF')
    GLOBAL_ANS = sorted(docs_score_tf, key=docs_score_tf.get, reverse=True)[:20]
    for i in sorted(BUFFER_OUT):
        logging.debug(i)
    for i in GLOBAL_ANS:
        file = open(os.path.join('Data', i), encoding='utf-8')
        text = file.read()
        file.close()
        print(i, 'TF:%.15f' % docs_score_tf[i], fileText in text.replace('\n', ''))

    GLOBAL_ANS = ['']
    logging.debug('TFIDF')
    GLOBAL_ANS = sorted(docs_score_tfidf, key=docs_score_tfidf.get, reverse=True)[:20]
    for i in sorted(BUFFER_OUT):
        logging.debug(i)
    for i in GLOBAL_ANS:
        file = open(os.path.join('Data', i), encoding='utf-8')
        text = file.read()
        file.close()
        print(i, 'TFIDF:%.15f' % docs_score_tfidf[i], fileText in text.replace('\n', ''))

    print('eclipsed time:', stop - start)
